<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ejercicio 3</title>
    </head>
    
    <body>
        <p>Empezando a trabajar en ficheros PHP: Don Quijote</p>

        <?php 
           function buscadorMolino($palabra) {
               $descriptor = fopen("donquijote.txt" , "r");
               $numeroPalabras = 0;

               while (($contenido = fgets($descriptor)) !== false) {
                    $numeroPalabras = $numeroPalabras + substr_count($contenido, $palabra);  //substr_count = contador de palabras
               }
               fclose($descriptor);
               
               echo "La palabra $palabra aparece en el texto $numeroPalabras veces en total";
           }
        ?>
    </body>
</html>
